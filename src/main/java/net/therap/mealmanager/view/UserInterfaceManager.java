package net.therap.mealmanager.view;

/**
 * @author arafat
 * @since 11/21/16
 */
public class UserInterfaceManager {

    public static void setHomeScreenUI(){
        String underlineVariableForView = "-----------------------------------------------" +
                "-----------------\n";

        System.out.printf("%s%40s%s", underlineVariableForView, "Weekly Meal Planner\n",
                underlineVariableForView);
        System.out.printf("%20s%s", "[1] ", "View Meal For Each Day\n");
        System.out.printf("%20s%s", "[2] ", "Manage Type of Meal\n");
        System.out.printf("%20s%s", "[3] ", "Manage Dishes\n");
        System.out.printf("%20s%s", "[4] ", "Prepare Menu\n");
        System.out.printf("%20s%s", "[0] ", "Exit\n\n");

        System.out.print("Press 1/2/3/4 to proceed: ");

    }

    public static void setManageMealTypeScreenUI(String underlineVariableForView){
        System.out.printf("%s%40s%s", underlineVariableForView, "Manage Meal type\n",
                underlineVariableForView);
        System.out.printf("%20s%s", "[A] ", "Add Meal Type\n");
        System.out.printf("%20s%s", "[V] ", "View All Meal Types\n");
        System.out.printf("%20s%s", "[B] ", "Go To Previous Menu\n");
    }

    public static void setPrepareMenuScreenUI(){
        String underlineVariableForView = "-----------------------------------------------" +
                "-----------------\n";

        System.out.printf("%s%40s%s", underlineVariableForView,"Prepare Menu\n",
                underlineVariableForView);
        System.out.printf("%20s%s%20s%s%20s%s%20s%s%20s%s%20s%s","[1] ", "Sunday\n", "[2] ", "Monday\n",
                "[3] ", "Tuesday\n", "[4] ", "Wednesday\n", "[5] ", "Thursday\n", "[0] ", " Go Back\n");
        System.out.print("Select Day: ");
    }

    public static void setViewAndEditMealScreenUI(String underlineVariableForView) {
        System.out.printf("%s%40s%s", underlineVariableForView, "View and Edit Panel\n",
                underlineVariableForView);
        System.out.printf("%20s%s%20s%s%20s%s%20s%s%20s%s%20s%s", "[1] ", "Sunday\n", "[2] ", "Monday\n",
                "[3] ", "Tuesday\n", "[4] ", "Wednesday\n", "[5] ", "Thursday\n", "[0] ", " Go Back\n");
        System.out.print("Select Day: ");
    }

}
