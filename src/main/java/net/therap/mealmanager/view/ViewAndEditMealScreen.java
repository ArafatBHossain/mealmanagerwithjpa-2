package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.DailyMeal;
import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.DailyMealService;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealTypeService;
import net.therap.mealmanager.validator.ObjectChecker;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ViewAndEditMealScreen extends ObjectChecker {

    private DailyMealService dailyMealService;

    private List<Dish> listOfDishByDayAndType;

    private String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";

    public void showMealForEachDay() throws SQLException, ClassNotFoundException {
        MealTypeService mealTypeService = new MealTypeService();
        dailyMealService = new DailyMealService();

        List<MealType> mealTypeList = mealTypeService.getAllMealType();
        listOfDishByDayAndType = new ArrayList<>();

        UserInterfaceManager.setViewAndEditMealScreenUI(underlineVariableForView);

        Scanner dayInputScanner = new Scanner(System.in);

        int weekDayId = dayInputScanner.nextInt();

        if (weekDayId == 0) {
            HomeScreen.setHomeScreen();

        } else {
            DailyMeal dailyMeal = dailyMealService.getDailyMealById(weekDayId);

            for (MealType mealType : mealTypeList) {
                System.out.printf("%22s%s","["+mealType.getId()+"] ", mealType.getName() + "\n");
            }

            System.out.print("\nSelect Meal Box: ");
            int inputTypeOfMeal = new Scanner(System.in).nextInt();

            MealType mealType = mealTypeService.getMealType(inputTypeOfMeal);

            listOfDishByDayAndType = getListOfDishForMealType(dailyMeal.getListOfDishes(), mealType.getName());

            showMenu(listOfDishByDayAndType, dailyMeal.getName());

            System.out.print("[Y]Home [N]Continue Viewing [A] Add dishes to menu [D] Delete dishes from menu : ");
            String decisionInput = new Scanner(System.in).next();

            switch (decisionInput) {
                case "Y":
                    HomeScreen.setHomeScreen();
                    break;

                case "N":
                    showMealForEachDay();
                    break;

                case "D":
                    System.out.print("Select ID to delete: ");
                    int dishIdToDelete = new Scanner(System.in).nextInt();
                    deleteDishFromMenu(dishIdToDelete,dailyMeal, mealType.getName());
                    break;

                case "A":
                    addDishToExistingMenu(dailyMeal, mealType);
                    break;
            }
        }
    }

    public void showMenu(List<Dish> dishList, String weekDay) {
        System.out.printf("%s%40s%s", underlineVariableForView,
                "Menu for " + weekDay+"\n", underlineVariableForView);
        for (Dish dish : dishList) {
            System.out.printf("%20s%15s","["+String.valueOf(dish.getId())+"]",dish.getName()+"\n");
        }
    }

    public void addDishToExistingMenu(DailyMeal dailyMeal, MealType mealType) throws SQLException,
            ClassNotFoundException {

        DishService dishService = new DishService();
        dailyMealService = new DailyMealService();

        List<Dish> allDishes = dishService.getAllDishes();
        List<Dish> listOfDishForThisDailyMeal = dailyMeal.getListOfDishes();

        for (Dish dish : allDishes) {
            System.out.printf("%20s%s","["+String.valueOf(dish.getId())+"] ",dish.getName()+"\n");
        }

        List<MealType> listMealType;
        while (true) {
            System.out.print("Enter ID to add dishes to menu (Press 0 to Stop Adding)");

            int dishIdInput = new Scanner(System.in).nextInt();
            if (dishIdInput == 0) {
                break;

            }else{
                Dish dish = dishService.getDish(dishIdInput);
                listMealType = dish.getMealTypeList();
                if(!hasMealType(listMealType,mealType)){
                    listMealType.add(mealType);
                    dish.setMealTypeList(listMealType);
                    dishService.updateDish(dish);
                }
                listOfDishForThisDailyMeal.add(dish);
            }

        }

        dailyMeal.setListOfDishes(listOfDishForThisDailyMeal);
        dailyMealService.updateDailyMeal(dailyMeal);
        showMenu(listOfDishForThisDailyMeal, dailyMeal.getName());
        showMealForEachDay();
    }

    public void deleteDishFromMenu(int dishId,DailyMeal dailyMeal, String mealTypeName) throws SQLException,
            ClassNotFoundException {
        dailyMealService = new DailyMealService();
        List<Dish> dishList = dailyMeal.getListOfDishes();
        for (int i = 0; i < dishList.size(); i++) {
            Dish dish = dishList.get(i);
            if (dish.getId()==dishId) {
                dishList.remove(i);
                i--;
            } else {
                System.out.println("Else Called");
            }
        }

        dailyMeal.setListOfDishes(dishList);
        dailyMealService.updateDailyMeal(dailyMeal);

        listOfDishByDayAndType = getListOfDishForMealType(dishList, mealTypeName);
        showMenu(listOfDishByDayAndType, dailyMeal.getName());
        showMealForEachDay();
    }

    public List<Dish> getListOfDishForMealType(List<Dish> listOfDishNotGroupedByMealType, String mealTypeName){
        System.out.println(listOfDishNotGroupedByMealType.size());
        List<Dish> dishListForMealType = new ArrayList<>();

        for(Dish dish: listOfDishNotGroupedByMealType){
            for(MealType mT : dish.getMealTypeList()){
                if(mT.getName().equals(mealTypeName)){
                    dishListForMealType.add(dish);
                    break;
                }
            }
        }

        return dishListForMealType;
    }



    public boolean hasMealType(List<MealType> listMealType, MealType mealType){

       return super.hasMealType(listMealType, mealType);

    }
}
