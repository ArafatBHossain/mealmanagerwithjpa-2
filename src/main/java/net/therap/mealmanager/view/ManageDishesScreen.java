package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.*;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ManageDishesScreen {

    private DishService dishService;
    private MealTypeService mealTypeService;

    private List<MealType> mealTypeList;

    private String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";

    public void manageDishes() throws SQLException, ClassNotFoundException {

        mealTypeService = new MealTypeService();
        dishService = new DishService();

        mealTypeList = mealTypeService.getAllMealType();
        Scanner scannerDishNameInput = new Scanner(System.in);
        String dishName;

        System.out.printf("%s%40s%s", underlineVariableForView, "Manage Dishes\n",
                underlineVariableForView);

        for (MealType mealType : mealTypeList) {
            System.out.printf("%22s%s","["+mealType.getId()+"] ",mealType.getName()+"\n");
        }

        System.out.print("Select Meal Type [Press 0 to go back]:");

        int dishInput = new Scanner(System.in).nextInt();

        if (dishInput == 0) {
            HomeScreen.setHomeScreen();

        } else {
            while (true) {
                Dish dish = new Dish();
                System.out.print("Enter dish name [Type Q to stop adding and quit]: ");
                dishName = scannerDishNameInput.nextLine();
                if (dishName.equals("Q")) {
                    break;

                } else {
                    dish.setName(dishName);
                    System.out.println(dish.getName());
                    dishService.saveDish(dish);
                }
            }
        }

        HomeScreen.setHomeScreen();
    }
}
