package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.DailyMeal;
import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.DailyMealService;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealTypeService;
import net.therap.mealmanager.validator.ObjectChecker;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class PrepareMenuScreen extends ObjectChecker {

    public void manageMenuForEachDay() throws SQLException, ClassNotFoundException {

        MealTypeService mealTypeService = new MealTypeService();
        DishService dishService = new DishService();
        DailyMealService dailyMealService = new DailyMealService();

        List<MealType> mealTypeList = mealTypeService.getAllMealType();

        UserInterfaceManager.setPrepareMenuScreenUI();

        Scanner dayInputScanner = new Scanner(System.in);
        int weekDayId = dayInputScanner.nextInt();

        DailyMeal dailyMeal = dailyMealService.getDailyMealById(weekDayId);


        if (weekDayId == 0) {
            HomeScreen.setHomeScreen();
        }

        for (MealType mealType : mealTypeList) {
            System.out.printf("%s%22s","["+mealType.getId()+"] ",mealType.getName()+"\n");
        }

        System.out.print("\n\nSelect Meal Box: ");

        int inputTypeOfMeal = new Scanner(System.in).nextInt();

        MealType mealType = mealTypeService.getMealType(inputTypeOfMeal);

        List<Dish> dishList = dishService.getAllDishes();

        for (Dish dish : dishList) {
            System.out.printf("%20s%15s","["+String.valueOf(dish.getId())+"]",dish.getName()+"\n");
        }

        Scanner inputDishScanner = new Scanner(System.in);

        List<Dish> dishToAdd = new ArrayList<>();
        List<MealType> listMealType;

        while (true) {
            System.out.print("Enter id to add dishes to menu[Press 0 to stop]: ");
            int dishId = inputDishScanner.nextInt();

            if(dishId==0){
                break;
            }else{
                Dish dish= dishService.getDish(dishId);
                listMealType = dish.getMealTypeList();
                boolean h = hasMealType(listMealType,mealType);
                System.out.println(h);
                if(hasMealType(listMealType, mealType)==false){
                    listMealType.add(mealType);
                    dish.setMealTypeList(listMealType);
                    dishService.updateDish(dish);

                }

                dishToAdd.add(dish);

            }

        }
        dailyMeal.setListOfDishes(dishToAdd);
        dailyMealService.updateDailyMeal(dailyMeal); //This line, to be exact, is triggering the error
        HomeScreen.setHomeScreen();
    }

    public boolean hasMealType(List<MealType> listMealType, MealType mealType){

        return super.hasMealType(listMealType, mealType);
    }
}
