package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ManageMealTypeScreen {

    private MealTypeService mealTypeService;

    private List<MealType> listOfMealType;

    String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";

    public void manageMealType() throws SQLException, ClassNotFoundException {
        mealTypeService = new MealTypeService();

        UserInterfaceManager.setManageMealTypeScreenUI(underlineVariableForView);

        Scanner sc = new Scanner(System.in);
        String input = sc.next();

        switch (input) {
            case "A":
                System.out.print("Please enter type of meal[B to go to previous menu]: ");
                Scanner scannerInputMealType = new Scanner(System.in);
                String mealTypeName = scannerInputMealType.nextLine();
                if(!mealTypeName.equals("B")){
                   mealTypeService.saveMealType(new MealType(mealTypeName));
                }else {
                    manageMealType();
                }
                System.out.println();
                manageMealType();

            case "V":
                listOfMealType = mealTypeService.getAllMealType();
                System.out.printf("%s%40s%s", underlineVariableForView, "Meal types\n",
                        underlineVariableForView);
                for (MealType mealType : listOfMealType) {
                    System.out.printf("%40s", mealType.getName() + "\n");
                }
                System.out.println();
                manageMealType();

            case "B":
                HomeScreen.setHomeScreen();
                System.out.println();
                manageMealType();

        }
    }
 }
