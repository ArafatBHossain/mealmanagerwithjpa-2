package net.therap.mealmanager.view;

import net.therap.mealmanager.utility.HibernateUtil;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class HomeScreen {

    public static void setHomeScreen() throws SQLException, ClassNotFoundException {

        UserInterfaceManager.setHomeScreenUI();

        Scanner scanner = new Scanner(System.in);

        int menuScreen = scanner.nextInt();

        switch (menuScreen) {
            case 1:
                new ViewAndEditMealScreen().showMealForEachDay();
                break;

            case 2:
                new ManageMealTypeScreen().manageMealType();
                break;

            case 3:
                new ManageDishesScreen().manageDishes();
                break;

            case 4:
                new PrepareMenuScreen().manageMenuForEachDay();
                break;

            case 0:
                HibernateUtil.closeSessionFactory();
                break;
        }
    }
}
