package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.DishDao;
import net.therap.mealmanager.domain.Dish;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public class DishService {

    private DishDao dishDao;

    public void saveDish(Dish dish) throws SQLException, ClassNotFoundException {
        dishDao = new DishDao();
        dishDao.save(dish);
    }

    public Dish getDish(int id){
        dishDao = new DishDao();
        return dishDao.findById(id);
    }

    public List<Dish> getAllDishes(){
        dishDao = new DishDao();
        return dishDao.findAll();
    }

    public void updateDish(Dish dish) throws SQLException, ClassNotFoundException {
        dishDao = new DishDao();
        dishDao.update(dish);
    }


}
