package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.DailyMealDao;
import net.therap.mealmanager.domain.DailyMeal;

/**
 * @author arafat
 * @since 11/20/16
 */
public class DailyMealService {

    DailyMealDao dailyMealDao;

    public DailyMeal getDailyMealById(int id){
        dailyMealDao = new DailyMealDao();
        return dailyMealDao.findById(id);
    }

    public void updateDailyMeal(DailyMeal day){
        dailyMealDao = new DailyMealDao();
        dailyMealDao.update(day);
    }

}
