package net.therap.mealmanager.validator;

import net.therap.mealmanager.domain.MealType;

import java.util.List;

/**
 * @author arafat
 * @since 11/23/16
 */
public class ObjectChecker {
    boolean hasType = false;

    public boolean hasMealType(List<MealType> listMealType, MealType mealType) {
        System.out.println("SIZE: "+listMealType.size());
        if(listMealType.size() == 0){
            return hasType;
        }
        for (MealType mT : listMealType) {
            if (mT.getName().equals(mealType.getName())) {
                //System.out.println("Same found");
                hasType = true;
            }
        }

        return hasType;

    }
}
