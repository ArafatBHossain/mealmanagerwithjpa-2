package net.therap.mealmanager.validator;

/**
 * @author arafat
 * @since 11/22/16
 */
public class Error {

    private String name;
    private String message;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
