package net.therap.mealmanager.main;

import net.therap.mealmanager.utility.HibernateUtil;
import net.therap.mealmanager.view.HomeScreen;
import java.sql.SQLException;

/**
 * @author arafat
 * @since 11/13/16
 */
public class MealManager {

    public static void main(String [] args) throws SQLException, ClassNotFoundException {

        HibernateUtil.buildSessionAnntationFactory();

        HomeScreen.setHomeScreen();
    }
}
