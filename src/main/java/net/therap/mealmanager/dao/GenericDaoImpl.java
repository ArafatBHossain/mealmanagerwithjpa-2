package net.therap.mealmanager.dao;

import net.therap.mealmanager.utility.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/20/16
 */
public class GenericDaoImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

   private SessionFactory sessionFactory;

   private Class<T> type;

   public GenericDaoImpl(Class<T> type){
       this.type = type;
       sessionFactory = HibernateUtil.getSessionAnnotationFactory();
   }

    @Override
    public T findById(ID id) throws RuntimeException {
        T ret = null;

        try{
            ret = findIdNativeType(id);

        }catch (Exception e){
            e.printStackTrace();
        }
        return ret;
    }


    @Override
    public List<T> findAll() throws RuntimeException {
        List<T> list = null;

        Session session = sessionFactory.getCurrentSession();
        Transaction txt = session.beginTransaction();
        try {
            list = session.createQuery("SELECT o FROM " + type.getName() + " o").list();
            txt.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
            return list;
    }

    @Override
    public void save(T object) throws RuntimeException {
        try{
            Session session = sessionFactory.getCurrentSession();

            Transaction tx = session.beginTransaction();
            session.save(object);
            tx.commit();
           // session.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(T object) throws RuntimeException {
        try{
            Session session = sessionFactory.getCurrentSession();

            Transaction tx = session.beginTransaction();

            session.merge(object);
            tx.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private T findIdNativeType(ID id) {
        T ret = null;

        if(id!=null){
            Session session = sessionFactory.openSession();
            Transaction txt = session.beginTransaction();
            ret = (T)session.get(type, id);
            txt.commit();
        }

        return ret;
    }

}
