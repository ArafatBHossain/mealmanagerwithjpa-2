package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.Dish;

/**
 * @author arafat
 * @since 11/20/16
 */
public class DishDao extends GenericDaoImpl<Dish, Integer> {

    public DishDao(){
        super(Dish.class);
    }

}
