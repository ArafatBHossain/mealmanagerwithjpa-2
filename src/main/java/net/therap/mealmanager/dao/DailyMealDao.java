package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.DailyMeal;

/**
 * @author arafat
 * @since 11/20/16
 */
public class DailyMealDao extends GenericDaoImpl<DailyMeal, Integer> {

    public DailyMealDao(){
        super(DailyMeal.class);
    }

  }
