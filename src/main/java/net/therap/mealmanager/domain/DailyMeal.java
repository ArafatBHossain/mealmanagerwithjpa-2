package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/14/16
 */
@Entity
@Table(name = "weekday")
public class DailyMeal implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "w_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "day")
    private String name;

    @ManyToMany(targetEntity = Dish.class, cascade = {CascadeType.ALL})
    @JoinTable(name = "wday_mtype_dish", joinColumns = {@JoinColumn(name = "w_id")},
            inverseJoinColumns = {@JoinColumn(name = "dish_id")})
    private List<Dish> listOfDishes;

    public DailyMeal(){

    }

    public DailyMeal(String name){
        this.setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int weekDayId) {
        this.id = weekDayId;
    }

    public String getName() {
        return name;
    }

    public void setName(String dayName) {
        this.name = dayName;
    }

    public List<Dish> getListOfDishes() {
        return listOfDishes;
    }

    public void setListOfDishes(List<Dish> listOfDishes) {
        this.listOfDishes = listOfDishes;
    }
}
