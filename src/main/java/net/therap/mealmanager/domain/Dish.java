package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
@Entity
@Table(name = "dish")
public class Dish implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "dish_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "dish_name")
    private String name;

    @ManyToMany(targetEntity = MealType.class, cascade = {CascadeType.ALL})
    @JoinTable(name = "dish_meal_type_table", joinColumns = {@JoinColumn(name = "dish_id")},
            inverseJoinColumns = {@JoinColumn(name = "meal_type_id")})
    private List<MealType> mealTypeList;


    public Dish(){}

    public int getId() {
        return id;
    }

    public void setId(int dishId) {
        this.id = dishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String dishName) {
        this.name = dishName;
    }

    public List<MealType> getMealTypeList() {
        return mealTypeList;
    }

    public void setMealTypeList(List<MealType> mealTypeList) {
        this.mealTypeList = mealTypeList;
    }

  }

