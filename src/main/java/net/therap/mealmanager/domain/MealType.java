package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
@Entity
@Table(name = "meal_type")
public class MealType implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "meal_type_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "meal_type_name")
    private String name;

    @ManyToMany(targetEntity = Dish.class, cascade = {CascadeType.ALL})
    @JoinTable(name = "dish_meal_type_table", joinColumns = {@JoinColumn(name = "meal_type_id")},
    inverseJoinColumns = {@JoinColumn(name = "dish_id")})
    private List<Dish> dishList;

    public MealType(){

    }

    public MealType(String name){
        this.setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int mealTypeId) {
        this.id = mealTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String mealTypeName) {
        this.name = mealTypeName;
    }

    public List<Dish> getDishList() {
        return dishList;
    }

    public void setDishList(List<Dish> dishList) {
        this.dishList = dishList;
    }

}
